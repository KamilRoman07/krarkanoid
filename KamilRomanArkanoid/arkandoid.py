import pygame
import random
from pygame.locals import *

pygame.init()

# Preparations
SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
clock = pygame.time.Clock()
pygame.display.set_caption("Kamil Roman Arkanoid")
global game_state
global game_playing
global lives
global player
global ball
global klocki
global points
global win
global highscores
global can_append
can_append = False
win = False
game_playing = False
game_state = "Menu"
uruchomiona = True
pygame.mixer.music.load("sounds/intro.mp3")
pygame.mixer.music.set_volume(0.2)
pygame.mixer.music.play(-1)


# Classes
class Player(pygame.sprite.Sprite):

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.surf = pygame.Surface((30, 10))
        self.surf.fill((255, 0, 0))
        self.surf = pygame.image.load("images/belka.png").convert()
        self.rect = self.surf.get_rect(center=(x, y))

    def update(self, pressed_keys):
        if pressed_keys[K_SPACE]:
            global game_playing
            game_playing = not game_playing

        if pressed_keys[K_LEFT] and game_playing:
            self.rect.move_ip(-3, 0)
        if pressed_keys[K_RIGHT] and game_playing:
            self.rect.move_ip(3, 0)


        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH

    def is_collided_with_ball(self, sprite):
        return self.rect.colliderect(sprite.rect)


class Klocek(pygame.sprite.Sprite):

    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.surf = pygame.Surface((20, 10))
        self.surf = pygame.image.load("images/klocek.png").convert()
        self.rect = self.surf.get_rect(center=(x, y))
        self.x = x
        self.y = y

    def set_kolor(self, color):
        self.surf.fill(color)


class Ball(pygame.sprite.Sprite):

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.surf = pygame.Surface((5, 5))
        self.surf.fill((255, 255, 255))
        self.rect = self.surf.get_rect(center=(x, y))

    def set_position(self, x, y):
        self.x = x
        self.y = y
        self.rect = self.surf.get_rect(center=(x, y))

    def get_position(self):
        return (self.x, self.y)

    def set_direction(self, x, y):
        self.directionx = x
        self.directiony = y

    def get_direction(self):
        return self.directionx, self.directionx


def generate_level_1():
    global player
    global ball
    global klocki
    global points
    global can_append
    can_append = True
    points = 0
    player = Player(305, 450)
    ball = Ball(315, 440)
    ball.set_direction(1, -1)
    klocki = pygame.sprite.Group()
    global lives
    lives = 3
    screen.fill((0, 0, 0))
    global game_state
    game_state = "Level1"
    x = 20
    y = 20
    while x < 640:
        while y < 140:
            klocek = Klocek(x, y)
            klocki.add(klocek)
            y = y + 10
        y = 20
        x = x + 20
    pass


def generate_level_2():
    global player
    global ball
    global klocki
    player = Player(305, 450)
    ball = Ball(315, 440)
    ball.set_direction(1, -1)
    klocki = pygame.sprite.Group()
    screen.fill((0, 0, 0))
    global game_state
    game_state = "Level2"
    x = 20
    y = 20
    allowedx = 40
    while y < 140:
        while x < allowedx:
            klocek = Klocek(x, y)
            klocki.add(klocek)
            x = x + 20
        y = y + 10
        allowedx = allowedx + 20 * 2.5
        x = 20
    pass


def generate_level_3():
    global player
    global ball
    global klocki
    player = Player(305, 450)
    ball = Ball(315, 440)
    ball.set_direction(1, -1)
    klocki = pygame.sprite.Group()
    screen.fill((0, 0, 0))
    global game_state
    game_state = "Level3"
    x = 60
    y = 20
    spawn = True
    while x < 580:
        while y < 140:
            if spawn:
                klocek = Klocek(x, y)
                klocki.add(klocek)
                spawn = False
            else:
                spawn = True
            y = y + 10
        spawn = not spawn
        y = 20
        x = x + 20
    pass


def generate_level_4():
    global player
    global ball
    global klocki
    player = Player(305, 450)
    ball = Ball(315, 440)
    ball.set_direction(1, -1)
    klocki = pygame.sprite.Group()
    screen.fill((0, 0, 0))
    global game_state
    game_state = "Level4"
    x = 20
    y = 20
    bpoint = 1
    while x < 640:
        while y < 140:
            if bpoint < 3:
                klocek = Klocek(x, y)
                klocki.add(klocek)
            y = y + 10
        if bpoint < 3:
            bpoint += 1
        else:
            bpoint = 1
        y = 20
        x = x + 20
    pass


def generate_level_5():
    global player
    global ball
    global klocki
    player = Player(305, 450)
    ball = Ball(315, 440)
    ball.set_direction(1, -1)
    klocki = pygame.sprite.Group()
    screen.fill((0, 0, 0))
    global game_state
    game_state = "Level5"
    x = 40
    y = 140
    while x < 620:
        while y < 280:
            if random.randint(0, 1) > 0:
                klocek = Klocek(x, y)
                klocki.add(klocek)
                print(x, y)
            y = y + 10
        y = 140
        x = x + 20
    pass

def update_ball_position():
    global ball
    global lives
    if ball.x <= 0 or ball.x >= SCREEN_WIDTH:
        ball.directionx = -ball.directionx
    if ball.y <= 0:
        ball.directiony = 1
    if ball.y >= SCREEN_HEIGHT:
        lives-=1
        reset_ball_position()
    ball.set_position(ball.x + ball.directionx, ball.y + ball.directiony)
    pass

def reset_ball_position():
    global ball
    global player
    global game_playing
    game_playing = False
    player = Player(305, 450)
    ball = Ball(315, 440)
    ball.set_direction(1, -1)

def end_screen():
    try:
        global points
        global highscores
        global can_append
        if can_append:
            with open("Highscores.txt", "a+") as file:
                file.write("%s" % points)
            can_append = False
            with open("Highscores.txt", "w+") as file:
                highscores = file.readlines()

    except IOError:
        print("None")
        with open("Highscores.txt", "w+") as file:
            global highscores
            highscores = file.readlines()
            pass
    global game_state
    game_state = "End_screen"
    pass

while uruchomiona:
    # Menu
    global points
    global game_state
    global highscores
    if game_state == "Menu":
        global win
        win = False
        screen.fill((0, 0, 0))
        font = pygame.font.SysFont(None, 56)
        hello = font.render("Arkanoid", True, (0, 255, 0))
        screen.blit(hello, (240, 20))

        font = pygame.font.SysFont(None, 24)
        hello = font.render("1. Start new game", True, (255, 255, 0))
        screen.blit(hello, (240, 250))

        hello = font.render("2. Best scores", True, (255, 255, 0))
        screen.blit(hello, (240, 300))

        hello = font.render("3. Quit", True, (255, 255, 0))
        screen.blit(hello, (240, 350))

        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE or event.key == K_3:
                    uruchomiona = False
                elif event.key == K_2:
                    end_screen()
                    pass
                elif event.key == K_1:
                    generate_level_1()

    if "Level" in game_state:
        global klocki
        global player
        global ball
        global win
        screen.fill((0, 0, 0))

        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    end_screen()

        if lives < 1:
            end_screen()
            win = False

        if not klocki:
            if game_state == "Level1":
                generate_level_2()
            elif game_state == "Leve2":
                generate_level_3()
            elif game_state == "Leve3":
                generate_level_4()
            elif game_state == "Leve4":
                generate_level_5()
            elif game_state == "Leve5":
                end_screen()
                win = True

        if game_playing:
            update_ball_position()

        for klocek in klocki:
            screen.blit(klocek.surf, klocek.rect)
        screen.blit(player.surf, player.rect)
        screen.blit(ball.surf, ball.rect)

        pygame.event.get()
        pressed_keys = pygame.key.get_pressed()
        player.update(pressed_keys)

        if pygame.sprite.spritecollideany(ball, klocki):
            delete_klocek = pygame.sprite.spritecollideany(ball, klocki)
            new_x = -(delete_klocek.x - ball.x)/5
            delete_klocek.kill()
            ball.directiony = -ball.directiony
            ball.directionx = new_x
            points += 10

        if player.is_collided_with_ball(ball):
            new_x = (player.rect.x - ball.x) / 10
            ball.directionx = -(new_x + 2)
            ball.directiony = -1

    if game_state == "End_screen":
        end_screen()
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_1:
                    game_state = "Menu"

        screen.fill((0, 0, 0))
        font = pygame.font.SysFont(None, 56)
        hello = font.render("Arkanoid", True, (0, 255, 0))
        screen.blit(hello, (240, 20))

        if win:
            font = pygame.font.SysFont(None, 36)
            message = font.render("You won!", True, (255, 255, 0))
            screen.blit(message, (260, 75))

        font = pygame.font.SysFont(None, 20)
        message = font.render("Press 1 to get back to menu", True, (255, 0, 0))
        screen.blit(message, (250, 460))

        if highscores:
            font = pygame.font.SysFont(None, 22)
            message = font.render("Highscores", True, (0, 0, 255))
            screen.blit(message, (250, 120))
            y = 140
            for line in highscores:
                message = font.render(line, True, (0, 0, 255))
                screen.blit(message, (250, y))
                y += 20
        else:
            font = pygame.font.SysFont(None, 18)
            message = font.render("There is no highscores yet", True, (0, 0, 255))
            screen.blit(message, (250, 120))
        pass

    clock.tick(180)
    pygame.display.flip()

pygame.quit()
